**WeGo Demand Side Common Services**
*Reed Gauthier, Kathryn Reck, Sergiy Ensary, Summer Klimko, Kyle Flett*

**About**
This repository contains the files used to make the demand side common services pages front end. 
This is what a WeGo customer will use to sign in to their WeGo account or to create a new account to access the landing page with all WeGo services.
It also contains the WeGo Media App where users will place their orders. 

**How to use this repository**
Start at index.html and either register or login. 
You will then be taken to the services home page where you can select "WeGo Media" and view our web application.

**Next Steps**
The registration page has been giving occasional 405 errors that are being worked on.
Password Hashing has given a 405 error but is being worked on.