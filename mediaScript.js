
// Program Description: A user fills out the order form, the order info is then put in a dictionary,
//                      the dictionary is turned into a JSON readable string, the string is then passed to
//                      runHTTPRequest.py
// Pre: The user has filled out the order form and pressed the submit button
// Post: A response is recieved from runHTTPRequest.py
function submitForm()
{

    var orderInfo = {"flag": "order", "username": "rgauthie", "contents": document.getElementById('contents').value, "address": document.getElementById('address').value,
        "tos": "media"};


    strOderInfo = JSON.stringify(orderInfo);


    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "https://demand.team21.softwareengineeringii.com/api/backend", true);
    xhttp.setRequestHeader("Content-type", "application/json");

    xhttp.onreadystatechange = function()
    {

        if(this.readyState == 4 && this.status == 200)
        {
            alert("Order went through successfully");window.location="/orders.html";
        }
    };


    xhttp.send(strOderInfo);

}

// Program Description: used with mediaApp.html when a user selects a tab it will open the related page (books.html,
//                      account.html, movies.html, or orders.html
// Pre: a user selects a tab to go to
// Post: the relevant tab link is opened
function openPage(pageName,elmnt,color) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].style.backgroundColor = "";
    }
    document.getElementById(pageName).style.display = "block";
    elmnt.style.backgroundColor = color;
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();