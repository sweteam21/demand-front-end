//Program Description: Display map visuals on the orders.html page
var austinMap = L.map('mapid').setView([30.267, -97.742], 12);


L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=sk.eyJ1IjoicmdhdXRoaWUiLCJhIjoiY2s2N2FubWgzMDc0ZzNmcXhmenkxa2Z2dSJ9.XtxViZvhOW30FlrgJHWjPA', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox/streets-v11'
}).addTo(austinMap);


var control = L.Routing.control({
    waypoints: [L.latLng(30.2326024,-97.7562269),L.latLng(30.200678,-97.7724825),L.latLng(30.2326024,-97.7562269)],
}).addTo(austinMap);

control.on('routeselected', function(e) {
    var route = e.route;
    var routeLine = L.polyline(route.coordinates, {color: 'blue'}).addTo(austinMap);
    var animatedMarker = L.animatedMarker(routeLine.getLatLngs(), {icon: mediaDeliveryVehicleIcon}).addTo(austinMap).bindPopup("On Route to 401 Little Texas Ln. --- \nDelivering 1 Book -> 'Book', 1 Movie -> 'Movie'");
});

var mediaDeliveryVehicleIcon = L.icon({
    iconUrl: 'https://image.flaticon.com/icons/svg/499/499612.svg',
    shadowUrl: 'https://pngriver.com/wp-content/uploads/2018/04/Download-Shadow-Free-Download-PNG.png',

    iconSize: [40, 40],
    shadowSize: [50, 45],
    iconAnchor: [20, 20],
    shadowAnchor: [25, 25],
    popupAnchor: [20, 0]
});