// Program Function: Takes information submitted in the new user form, inserts info into a dictionary,
//                   turns dictionary into a JSON readable string, creates a new XMLHttpRequest, sends the
//                   JSON readable string to runHTTPRequest.py
// Pre: New user form is filled out and the user presses submit button
// Post: A response message is recieved fom runHTTPRequest.py
function submitForm()
{

    var loginInfo = {"flag": "regiser", "username": document.getElementById('user').value, "password": document.getElementById('pass').value, "firstName": document.getElementById('firstName').value, "lastName": document.getElementById('lastName').value, "email": document.getElementById('email').value};


    strLoginInfo = JSON.stringify(loginInfo);


    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "https://demand.team21.softwareengineeringii.com/api/backend", true);
    xhttp.setRequestHeader("Content-type", "header1");

    xhttp.onreadystatechange = function()
    {

        if(this.readyState == 4 && this.status == 200)
        {
            alert("Successfully Registered");window.location="/index.html";
        }
    };


    xhttp.send(strLoginInfo);


}